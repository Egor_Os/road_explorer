FROM python:3.6.8

RUN apt-get update && \
	apt-get install -y \
	ca-certificates \
	apt-transport-https \
	nginx supervisor && \
	apt-get clean && apt-get autoclean

COPY requirements.txt .

RUN pip3 install -r requirements.txt
RUN pip3 install uwsgi
RUN pip3 install https://download.pytorch.org/whl/cpu/torch-1.1.0-cp36-cp36m-linux_x86_64.whl
RUN pip3 install https://download.pytorch.org/whl/cpu/torchvision-0.3.0-cp36-cp36m-linux_x86_64.whl

# Make NGINX run on the foreground
RUN echo "daemon off;" » /etc/nginx/nginx.conf

COPY web_app /app/web_app
COPY explorer /app/explorer

COPY nginx.conf /etc/nginx/sites-available/default
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf
COPY uwsgi.ini /app/uwsgi.ini
COPY start.sh /app/start.sh
RUN chmod a+rwx /app/start.sh
WORKDIR /app
EXPOSE 80

ENV PYTHONPATH="/app/explorer"

CMD ["/bin/sh", "start.sh"]