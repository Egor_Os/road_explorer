import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';
import HeatMap from './components/HeatMap';


class App extends Component {
  render() {
    return (
      <div className="App">
        <HeatMap/>
      </div>
    );
  }
}

export default App;