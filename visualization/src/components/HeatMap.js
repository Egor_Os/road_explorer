 /* global google */
import React, { Component } from 'react'
import GoogleMapReact from 'google-map-react'

import './HeatMap.css'


function httpGetAsync(theUrl, callback) { //theURL or a path to file
    var httpRequest = new XMLHttpRequest();
    httpRequest.onreadystatechange = function() {
        if (httpRequest.readyState == 4 && httpRequest.status == 200) {
            var response = httpRequest.responseText;  //if you fetch a file you can JSON.parse(httpRequest.responseText)
            if (callback) {
                callback(response);
            } 
        }
    };

    httpRequest.open('GET', theUrl, true); 
    httpRequest.send(null);
}

// httpGetAsync('http://localhost:8000/api/points/?format=json', function(data) {
//     console.log(data)
// });

class HeatMap extends Component {
  static defaultProps = {
    // set initial state
    center: {
      lat: 59.561860508899485,
      lng: 30.077434255503015
    },
    zoom: 11
  }

  constructor(props) {
  	super(props)
  	this.state = {
      heatmapVisible: true,
  		heatmapPoints: [
		  	// 	{lat: 59.95, lng: 30.33},
					// {lat: 59.96, lng: 30.32}
				]
  	}
  }

  onMapClick({x, y, lat, lng, event}) {
   //  if (!this.state.heatmapVisible) {
   //    return
   //  }
    
  	// this.setState({
  	// 	heatmapPoints: [ ...this.state.heatmapPoints, {lat, lng}]
  	// })
   //  if (this._googleMap !== undefined) {      
   //    const point = new google.maps.LatLng(lat, lng)
   //    this._googleMap.heatmap.data.push(point)
   //  }
  }

  componentDidMount(){
    this.interval = setInterval(()=>{
      httpGetAsync('http://localhost:8000/api/points/?format=json', (response)=> {

        this._googleMap.heatmap.data.j.length = 0;
        JSON.parse(response).forEach((pt, index, array)=>{
            const point = {location: new google.maps.LatLng(pt.lat, pt.lng), weight: pt.overall_weight};
            this._googleMap.heatmap.data.push(point);
            this.setState({
              heatmapPoints: [ ...this.state.heatmapPoints, {lat:pt.lat, lng:pt.lng, weight: pt.overall_weight}]
            })
        })
      });
    }, 5000)
  }

  toggleHeatMap() {
    this.setState({
      heatmapVisible: !this.state.heatmapVisible
    }, () => {
      if (this._googleMap !== undefined) {
        this._googleMap.heatmap.setMap(this.state.heatmapVisible ? this._googleMap.map_ : null)
      }      
    })

  }

  render() {
    const apiKey = {key: 'AIzaSyD6b-wIm2AGAG300pr8lmDt2thFohZV9aw'}
  	const heatMapData = {
  		positions: this.state.heatmapPoints,
		options: {
			radius: 20,
			opacity: 0.8,
      gradient: [
          'rgba(0, 255, 255, 0)',
          'rgba(0, 255, 255, 1)',
          'rgba(0, 191, 255, 1)',
          'rgba(0, 127, 255, 1)',
          'rgba(0, 63, 255, 1)',
          'rgba(0, 0, 255, 1)',
          'rgba(0, 0, 223, 1)',
          'rgba(0, 0, 191, 1)',
          'rgba(0, 0, 159, 1)',
          'rgba(0, 0, 127, 1)',
          'rgba(63, 0, 91, 1)',
          'rgba(127, 0, 63, 1)',
          'rgba(191, 0, 31, 1)',
          'rgba(255, 0, 0, 1)'
        ]
		}
  	}

    return (
      <div style={{ height: '100vh', width: '100%' }}>
        <GoogleMapReact
          ref={(el) => this._googleMap = el}
          bootstrapURLKeys={apiKey}
          defaultCenter={this.props.center}
          defaultZoom={this.props.zoom}
          heatmapLibrary={true}
          heatmap={heatMapData}
          onClick={this.onMapClick.bind(this)}
        >
        </GoogleMapReact>
        <button className="toggleButton" onClick={this.toggleHeatMap.bind(this)}>Toggle heatmap</button>
      </div>
    )
  }
}

export default HeatMap
