#!/bin/sh

python3 web_app/heattheroad/manage.py migrate && python3 web_app/heattheroad/manage.py collectstatic --noinput && /usr/bin/supervisord -c /etc/supervisor/conf.d/supervisord.conf