from django.db import models


class Point(models.Model):
	lat = models.FloatField(db_index=True)
	lng = models.FloatField(db_index=True)
	heading = models.FloatField(null=True)
	pano_id = models.CharField(max_length=32, null=True)
	small_pit_weight = models.IntegerField(null=True)
	big_pit_weight = models.IntegerField(null=True)
	puddle_weight = models.IntegerField(null=True)
	crack_weight = models.IntegerField(null=True)
	offroad_weight = models.IntegerField(null=True)
	overall_weight = models.FloatField(null=True)
	updated_at = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return f'({self.lat}, {self.lng} : {self.overall_weight})'
