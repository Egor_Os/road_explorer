from django.urls import re_path, include
from rest_framework.routers import DefaultRouter

from .views import PointViewSet, CrawlView


router = DefaultRouter()
router.register(r'points', PointViewSet)

urlpatterns = [
    re_path(r'', include(router.urls)),
    re_path(r'^crawl/$', CrawlView.as_view())
]
