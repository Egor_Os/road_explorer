from django.conf import settings
from redis import Redis
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet
from rest_framework.mixins import ListModelMixin
from rest_framework.response import Response
from rq import Queue

from .crawler import Crawler
from .models import Point
from .serializers import PointSerializer, CrawlSerializer


class PointViewSet(ListModelMixin, GenericViewSet):
	queryset = Point.objects.all()
	serializer_class = PointSerializer

	def get_queryset(self):
		(swlat, swlng), (nelat, nelng) = self._get_bounds()
		return super().get_queryset().filter(
			lat__range=(swlat, nelat), lng__range=(swlng, nelng))

	def _get_bounds(self):
		try:
			sw, ne = self.request.queryset['bounds'].split(';')
			swlat, swlng = sw.split(',')
			nelat, nelng = ne.split(',')
		except Exception:
			(swlat, swlng), (nelat, nelng) = settings.DEFAULT_BOUNDS
		return (swlat, swlng), (nelat, nelng)


class CrawlView(APIView):
	def post(self, request):
		serializer = CrawlSerializer(data=request.data)
		serializer.is_valid(raise_exception=True)
		lat = serializer.data['lat']
		lng = serializer.data['lng']
		redis_conn = Redis(**settings.REDIS_CONN)
		q = Queue(is_async=settings.IS_ASYNC, connection=redis_conn)
		crawler = Crawler(
			(lat, lng), class_names_file='points/class_names.txt')
		q.enqueue(crawler.start_crawling)
		return Response(
			{'detail': f'Crawling started at ({lat}, {lng})'})
