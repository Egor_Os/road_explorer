from django.conf import settings
from rest_framework import serializers

from .models import Point


class PointSerializer(serializers.ModelSerializer):
	class Meta:
		model = Point
		fields = (
			'lat', 'lng',
			'small_pit_weight',
			'big_pit_weight',
			'puddle_weight',
			'crack_weight',
			'offroad_weight',
			'overall_weight',
		)


class CrawlSerializer(serializers.Serializer):
	lat = serializers.FloatField(
		min_value=-90.0, max_value=90.0,
		default=settings.DEFAULT_START_COORDS[0]
	)
	lng = serializers.FloatField(
		min_value=-180.0, max_value=180.0,
		default=settings.DEFAULT_START_COORDS[1]
	)
