import logging
import time
from collections import Counter
from detector import ObjectDetector
from django.conf import settings
from explorer import RoadExplorer
from caching import CacheProcessor, DISABLE_CACHE, RECORD_CACHE


logger = logging.getLogger('web')


class Crawler:
    zero_aggregation = {
        'small_pit': 1,
        'big_pit': 1,
        'puddle': 1,
        'crack': 1,
        'offroad': 1,
    }
    _model = None

    def __init__(
        self,
        start_coords=settings.DEFAULT_START_COORDS,
        google_api_token=settings.GOOGLE_API_TOKEN,
        cache_processor=None,
        object_detector=None,
        heading=130,
        street_view_size='640x640',
        step=0.0002,
        class_names_file=None,
        period=settings.DEFAULT_CRAWLING_PERIOD
    ):

        self.start_coords = start_coords
        self.google_api_token = google_api_token
        self.cache_processor = cache_processor or CacheProcessor(
            'cache', [RECORD_CACHE, DISABLE_CACHE])
        self.road_explorer =  RoadExplorer(
            self.google_api_token, cache_processor=self.cache_processor)
        self.object_detector = object_detector or ObjectDetector(
            settings.CONFIG_PATH,
            settings.WEIGHTS_PATH,
            settings.CLASS_PATH,
            settings.IMAGE_SIZE
        )
        self.heading = heading
        self.street_view_size = street_view_size
        self.step = step
        self.period = period
        self.indices_to_names_map = {}
        class_names_file = class_names_file or self.object_detector.class_path
        with open(class_names_file, 'r') as file:
            index = 0
            for class_name in file:
                self.indices_to_names_map[index] = class_name.strip()
                index += 1
        print(self.indices_to_names_map)

    def start_crawling(self):
        self.object_detector.load_model()

        visited = set()
        lat, lng = self.start_coords
        threshold = time.perf_counter() + self.period
        points_ids = []
        while time.perf_counter() < threshold:
            locations = self.road_explorer.get_closest_locations(
                lat, lng, self.step, self.google_api_token)
            for loc in locations:
                try:
                    if loc['pano_id'] in visited:
                        continue
                    else:
                        new_lat, new_lng = loc['coord']
                        street_view = self.road_explorer.get_street_view(
                            new_lat, new_lng, self.heading,
                            self.street_view_size,self.google_api_token
                        )
                        detections = self.object_detector.get_detections(
                            street_view)
                        result = self._parse_detections(detections)
                        print(result)
                        instance = self.save_to_db(
                            lat, lng, self.heading, loc['pano_id'],
                            aggregation=result
                        )
                        points_ids.append(instance.id)
                        print(instance)
                        visited.add(loc['pano_id'])
                        break
                except Exception as e:
                    logger.error(
                        'Exception while analyzing point (%s, %s):\n%s',
                        new_lat, new_lng, e, exc_info=True
                    )

            lat, lng = new_lat, new_lng

    def _parse_detections(self, detections):
        classes = []
        if detections is not None:
            for item in detections:
                class_index = int(item[-1].tolist())
                if class_index in self.indices_to_names_map:
                    classes.append(self.indices_to_names_map[class_index])
        return dict(Counter(self.zero_aggregation) + Counter(classes))

    def save_to_db(
        self, lat, lng,
        heading=None,
        pano_id=None,
        aggregation=None,
        overall_weight=None
    ):
        if aggregation is None:
            raise ValueError(
                'Please provide weights data as aggregation keyword argument')
        if overall_weight is None:
            values = [v for v in aggregation.values() if v is not None]
            overall_weight = sum(values) / len(values)
        # rename keys in aggregation
        aggregation = {
            'small_pit_weight': aggregation.get(
                'small_pit', self.zero_aggregation['small_pit']),
            'big_pit_weight': aggregation.get(
                'big_pit', self.zero_aggregation['big_pit']),
            'puddle_weight': aggregation.get(
                'puddle', self.zero_aggregation['puddle']),
            'crack_weight': aggregation.get(
                'crack', self.zero_aggregation['crack']),
            'offroad_weight': aggregation.get(
                'offroad', self.zero_aggregation['offroad']),
        }
        obj, _ = self.model.objects.update_or_create(
            lat=lat, lng=lng, defaults=dict(
                heading=heading, pano_id=pano_id,
                overall_weight=overall_weight, **aggregation
            )
        )
        return obj

    @property
    def model(self):
        """
        This property assures django is setup before accessing models,
        as this code is supposed to be executed in rq worker.
        """

        if self._model is None:
            from django import setup
            setup()
            from points.models import Point
            self._model=Point
        return self._model

    @classmethod
    def crawl(
        cls,
        start_coords=settings.DEFAULT_START_COORDS,
        google_api_token=settings.GOOGLE_API_TOKEN,
        cache_processor_args=None,
        object_detector_args=None,
        heading=130,
        street_view_size='640x640',
        step=0.0002,
        class_names_file=None,
        period=60  # in seconds
    ):
        """
        Classmethod that provides a way to initiate crawling using only
        easily serializable arguments.
        """
        if cache_processor_args is None:
            cache_processor = CacheProcessor(
                'cache', [RECORD_CACHE, DISABLE_CACHE])
        else:
            cache_processor = CacheProcessor(*cache_processor_args)
        if object_detector_args is None:
            object_detector = ObjectDetector(
                settings.CONFIG_PATH,
                settings.WEIGHTS_PATH,
                settings.CLASS_PATH,
                settings.IMAGE_SIZE
            )
        else:
            object_detector = ObjectDetector(*object_detector_args)
        crawler = cls(
            start_coords=start_coords,
            google_api_token=google_api_token,
            cache_processor=cache_processor,
            object_detector=object_detector,
            heading=heading,
            street_view_size=street_view_size,
            step=step,
            class_names_file=class_names_file,
            period=period  # in seconds
        )
        crawler.start_crawling()
