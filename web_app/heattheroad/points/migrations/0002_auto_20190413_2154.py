# Generated by Django 2.2 on 2019-04-13 18:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('points', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='point',
            name='big_pit_weight',
            field=models.IntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='point',
            name='crack_weight',
            field=models.IntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='point',
            name='heading',
            field=models.FloatField(null=True),
        ),
        migrations.AlterField(
            model_name='point',
            name='offroad_weight',
            field=models.IntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='point',
            name='overall_weight',
            field=models.IntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='point',
            name='pano_id',
            field=models.CharField(max_length=32, null=True),
        ),
        migrations.AlterField(
            model_name='point',
            name='puddle_weight',
            field=models.IntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='point',
            name='small_pit_weight',
            field=models.IntegerField(null=True),
        ),
    ]
