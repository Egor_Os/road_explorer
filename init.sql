CREATE USER heatadmin WITH PASSWORD 'heatadmin';
CREATE DATABASE heattheroad;
GRANT ALL PRIVILEGES ON DATABASE heattheroad TO heatadmin;