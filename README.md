# Setup

## Virtual env


```
virtualenv --python=python3.6 venv
pip install -r requirements
pip install torch torchvision
```

## NodeJS

```
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
sudo apt-get install -y nodejs
```

If installation fails, [manual installation](
https://github.com/nodesource/distributions/blob/master/README.md#manual-installation) might help

## React

```
sudo npm install -g create-react-app
```

# Usage

## Run explorer

```
source venv/bin/activate
python explorer.py
```

## Run React app

```
npm start
```
``` source venv/bin/activate ```

# Run web app HeatTheRoad

1. install redis on your machine https://redis.io/download#installation
2. run redis server
3. install postgres on your machine https://www.postgresql.org/download/
4. create user:
``` sudo -u postgres psql ```

```sql
CREATE USER heatadmin WITH PASSWORD 'heatadmin';
CREATE DATABASE heattheroad;
GRANT ALL PRIVILEGES ON DATABASE heattheroad TO heatadmin;
```

5. add explorer dependencies to your virtual environment by adding explorer.pth file to your site-packages folder in environment with string indicating path to explorer folder of this repo

6. Run django with commands from web_app/heattheroad folder:
```python manage.py migrate```
```python manage.py runserver```

7. to run rq worker run commands in web_app/heattheroad folder:
```export DJANGO_SETTINGS_MODULE=heattheroad.settings```
```rq worker```
