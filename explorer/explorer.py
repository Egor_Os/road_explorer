#!/usr/bin/env python3

import os
import numpy as np
import math
import cv2
import urllib.request
import json
from request_templates import LOCATION_INFO_TP, MAP_VIEW_TP, STREET_VIEW_TP
from caching import CacheProcessor, \
    DISABLE_CACHE, RECORD_CACHE, USE_CACHE, USE_CACHE_ONLY
from detector import ObjectDetector


def get_distance(lat1, lon1, lat2, lon2):
    """ Calculates distance in meters between two points on Earth """
    R = 6378.137
    pi = math.pi
    dlat = lat2 * pi / 180 - lat1 * pi / 180
    dlon = lon2 * pi / 180 - lon1 * pi / 180
    a = math.sin(dlat/2) * math.sin(dlat/2) + \
    math.cos(lat1 * pi / 180) * math.cos(lat2 * pi / 180) * \
    math.sin(dlon/2) * math.sin(dlon/2)
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
    d = R * c
    return d * 1000


class RoadExplorer:

    def __init__(self, auth_key, cache_processor):
        self.auth_key = auth_key
        self.cache_processor = cache_processor

    def get_meta_info(self, latitude, longitude, auth_key):
        """
        Get META info about the place
        https://developers.google.com/maps/documentation/streetview/metadata
        :returns {
                   "copyright" : <copyright info>,
                   "date" : "<date when image was taken>",
                   "location" : {
                      "lat" : <float>,
                      "lng" : <float>
                   },
                   "pano_id" : <string, the unique id of given viewpoint>,
                   "status" : <status message whether or not image of this
                   location exists>
                }
        """
        request = LOCATION_INFO_TP.substitute(latitude=latitude,
                                          longitude=longitude,
                                          auth_key=auth_key)
        resp = urllib.request.urlopen(request)
        info = json.load(resp)
        return info

    def get_map_view(self, latitude, longitude, zoom, size, auth_key):
        """ Returns schematic image of a map, look from above """
        request = MAP_VIEW_TP.substitute(latitude=latitude,
                                          longitude=longitude,
                                          zoom=zoom,
                                          size=size,
                                          auth_key=auth_key)
        info = self.get_meta_info(latitude, longitude, auth_key)
        img = self.cache_processor.process_request(request, info)
        return img

    def get_street_view(self, latitude, longitude, heading, size, auth_key):
        """ Returns street view image of given location and orientation """
        # Might also add field of view (FoV) parameter
        # and advanced camera rotation parameters
        request = STREET_VIEW_TP.substitute(latitude=latitude,
                                             longitude=longitude,
                                             heading=heading,
                                             size=size,
                                             auth_key=auth_key)
        info = self.get_meta_info(latitude, longitude, auth_key)
        img = self.cache_processor.process_request(request, info)
        return img

    def get_closest_locations(self, latitude, longitude, r, auth_key):
        """
        Looks  for the closest viewpoints within given radius
        r = 0.0001 is best for not skipping any viewpoints, but sometimes
        can stuck
        r = 0.0002 prevents from being stuck, but may skip some viewpoints
        """
        # later on directions may be defined as parameter
        # in order to search viewpoints only in given directions
        # thus reducing the number of API calls and
        # increasing processing speed
        directions = [0, 45, 90, 135, 180, 225, 270, 315]
        pi = math.pi
        pts = [(r*round(math.cos(d*pi/180), 5),
                r*round(math.sin(d*pi/180), 5)) for d in directions]

        reference_info = self.get_meta_info(latitude,
                                            longitude,
                                            auth_key)
        reference_pano_id = reference_info['pano_id']

        locations = []
        for pt in pts:
            x, y = pt
            info = self.get_meta_info(latitude + x,
                                        longitude + y,
                                        auth_key)
            if info.get('pano_id'):
                pano_id = info['pano_id']

                # ignore id that represents reference point
                if pano_id == reference_pano_id:
                    continue

                coord = (info['location']['lat'], info['location']['lng'])
                location = {'pano_id': pano_id, 'coord': coord}

                # avoid duplicates
                if not location in locations:
                    locations.append(location)

        sorted_locations = sorted(locations, key=lambda x:
        get_distance(latitude, longitude, *x['coord']))
        return sorted_locations


def demo_1():
    """ Moving on pre-defined trajectory """
    cache = CacheProcessor('cache', [RECORD_CACHE, USE_CACHE])
    road_exp = RoadExplorer('wut', cache_processor=cache)
    demo_trajectory = [
        (59.563381, 30.0967867, 320),
        (59.5635148,30.0965469, 325),
        (59.5636474,30.0963111, 325),
        (59.5637751,30.0960741, 325),
        (59.5638584,30.0959812, 351),
        (59.5638584,30.0959812, 51),
        (59.5638584,30.0959812, 114),
        (59.5638584,30.0959812, 144),
        (59.5637751,30.0960741, 144),
        (59.5636474,30.0963111, 143),
        (59.563381, 30.0967867, 143),
        (59.5633179,30.0969146, 122),
        (59.5632494,30.0970559, 122),
        (59.5632115,30.0971346, 105),
        (59.5632121,30.0973088, 37),
        (59.5632971,30.0973625, 20),
        (59.5634258,30.0974312, 20),
        (59.563508,30.0974743, 20),
        (59.5635913,30.0975175, 24),
        (59.5636231,30.0976709, 97),
        (59.5635824,30.0978406, 98),
        (59.5635812,30.098076, 111)
    ]
    with open('token.txt', 'r') as f:
        auth_key = next(f)
    for pos in demo_trajectory:
        lat, long, heading = pos
        # top_view = road_exp.get_map_view(lat, long, 19, '600x600', auth_key)
        street_view = road_exp.get_street_view(lat, long, heading, '640x640',
                                               auth_key)

        # cv2.imshow('top view', top_view)
        cv2.imshow('street view', street_view)
        cv2.waitKey(300)


def demo_2():
    """ Moving between closest viewpoints """
    cache = CacheProcessor('cache', [RECORD_CACHE, USE_CACHE])
    road_exp = RoadExplorer('wut', cache_processor=cache)
    with open('token.txt', 'r') as f:
        auth_key = next(f)
    start = (59.5620955, 30.0991241)
    # start = (59.5586964,30.107389)
    heading = 130
    lat, lon = start
    visited = ['ZOSwNdN0DgzePbK-dC5R7A']
    for i in range(25):
        locations = road_exp.get_closest_locations(lat, lon, 0.0002,
                                                   auth_key)
        for loc in locations:
            if loc['pano_id'] in visited:
                continue
            else:
                new_lat, new_lon = loc['coord']
                street_view = road_exp.get_street_view(new_lat, new_lon,
                                                       heading, '640x640',
                                                       auth_key)
                cv2.imshow('street view', street_view)
                cv2.waitKey(200)
                visited.append(loc['pano_id'])
                break

        lat, lon = new_lat, new_lon


def demo_3():
    """ Image segmentation """
    cache = CacheProcessor('cache', [RECORD_CACHE, USE_CACHE])
    road_exp = RoadExplorer('wut', cache_processor=cache)
    with open('token.txt', 'r') as f:
        auth_key = next(f)

    config_path = 'model/yolov3.cfg'
    weights_path = 'model/yolov3.weights'
    class_path = 'model/coco.names'
    img_size = 416
    od = ObjectDetector(config_path, weights_path, class_path, img_size)
    od.load_model()

    start = (59.5620955, 30.0991241)
    # start = (59.5586964,30.107389)
    heading = 130
    lat, lon = start
    visited = ['ZOSwNdN0DgzePbK-dC5R7A']
    for i in range(25):
        locations = road_exp.get_closest_locations(lat, lon, 0.0002,
                                                   auth_key)
        for loc in locations:
            if loc['pano_id'] in visited:
                continue
            else:
                new_lat, new_lon = loc['coord']
                street_view = road_exp.get_street_view(new_lat, new_lon,
                                                       heading, '640x640',
                                                       auth_key)

                detections = od.get_detections(street_view)
                det_img = od.show_detections(street_view, detections)

                cv2.imshow('street view', det_img)
                cv2.waitKey(10)
                visited.append(loc['pano_id'])
                break

        lat, lon = new_lat, new_lon


if __name__ == '__main__':
    demo_3()
