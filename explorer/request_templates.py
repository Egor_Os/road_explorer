from string import Template
LOCATION_INFO_TP = Template('https://maps.googleapis.com/maps/api/streetview/metadata?location=$latitude,$longitude&key=$auth_key')
MAP_VIEW_TP = Template('https://maps.googleapis.com/maps/api/staticmap?center=$latitude,$longitude&zoom=$zoom&size=$size&maptype=roadmap&key=$auth_key')
STREET_VIEW_TP = Template('https://maps.googleapis.com/maps/api/streetview?location=$latitude,$longitude&heading=$heading&size=$size&key=$auth_key')