import torch
from pytorch_yolo_v3.models import Darknet
from pytorch_yolo_v3.utils.utils import load_classes, non_max_suppression
from torchvision import transforms
from torch.autograd import Variable
import cv2
from PIL import Image
import numpy as np
from random import randint
import logging


# Ignore warnings
import warnings
warnings.filterwarnings("ignore")


class ObjectDetector:

    def __init__(self, config_path, weights_path, class_path,
                 img_size=416, conf_thres=0.8, nms_thres=0.4):

        self.config_path = config_path
        self.weights_path = weights_path
        self.class_path = class_path
        self.img_size = img_size
        self.conf_thres = conf_thres
        self.nms_thres = nms_thres
        self.model = None
        self.CUDA = None
        self.Tensor = None
        self.classes = None
        self.colors = None

    def load_model(self):
        if self.model:
            return
        self.model = Darknet(self.config_path,
                             img_size=self.img_size)
        self.model.load_weights(self.weights_path)
        self.CUDA = torch.cuda.is_available()
        if self.CUDA:
            self.model.cuda()
        else:
            self.model.cpu()
        self.Tensor = torch.cuda.FloatTensor if self.CUDA \
            else torch.FloatTensor
        self.model.eval()
        self.classes = load_classes(self.class_path)
        self.colors = [(randint(0,200),
                        randint(0, 200),
                        randint(0,200)) for idx in self.classes]

    def __check_model(self):
        if self.model is None:
            raise RuntimeError("Model is not loaded")

    @staticmethod
    def convert_to_PIL(img):
        """Convert to <class 'PIL.Image.Image'>"""
        img = cv2.cvtColor(img,cv2.COLOR_BGR2RGB)
        return Image.fromarray(img)

    def get_detections(self, img):
        self.__check_model()

        if img is not Image.Image:
            img = self.convert_to_PIL(img)

        # scale and pad image
        ratio = min(self.img_size / img.size[0], self.img_size / img.size[1])
        imw = round(img.size[0] * ratio)
        imh = round(img.size[1] * ratio)
        img_transforms = transforms.Compose([transforms.Resize((imh, imw)),
                                             transforms.Pad((max(
                                                 int((imh - imw) / 2), 0), max(
                                                 int((imw - imh) / 2), 0), max(
                                                 int((imh - imw) / 2), 0), max(
                                                 int((imw - imh) / 2), 0)),
                                                            (128, 128, 128)),
                                             transforms.ToTensor(),
                                             ])
        # convert image to Tensor
        image_tensor = img_transforms(img).float()
        image_tensor = image_tensor.unsqueeze_(0)
        input_img = Variable(image_tensor.type(self.Tensor))
        # run inference on the model and get detections
        with torch.no_grad():
            detections = self.model(input_img)
            detections = non_max_suppression(detections, 80, self.conf_thres,
                                             self.nms_thres)
        return detections[0]

    def count_classes(self, detections):
        det_count = {cls: 0 for cls in self.classes}
        if detections is not None :
            for det in detections:
                class_idx = int(det[-1])
                class_name = self.classes[class_idx]
                det_count[class_name] += 1
        return det_count

    def show_detections(self, img, detections):
        det_img = np.array(img)

        pad_x = max(img.shape[0] - img.shape[1], 0) * (
        self.img_size / max(img.shape))
        pad_y = max(img.shape[1] - img.shape[0], 0) * (
        self.img_size / max(img.shape))
        unpad_h = self.img_size - pad_y
        unpad_w = self.img_size - pad_x

        if detections is not None:
            for x1, y1, x2, y2, conf, cls_conf, cls_pred in detections:
                class_idx = int(cls_pred)
                class_name = self.classes[class_idx]
                box_h = ((y2 - y1) / unpad_h) * img.shape[0]
                box_w = ((x2 - x1) / unpad_w) * img.shape[1]
                y1 = ((y1 - pad_y // 2) / unpad_h) * img.shape[0]
                x1 = ((x1 - pad_x // 2) / unpad_w) * img.shape[1]
                fill_color = self.colors[class_idx]
                cv2.rectangle(det_img, (x1, y1),
                              (x1+box_w, y1+box_h), fill_color, 3)
                cv2.rectangle(det_img, (x1, y1), (x1+box_w, y1+15),
                              fill_color, cv2.FILLED)
                cv2.cv2.putText(det_img,
                                class_name,
                                (x1+12, y1+12),
                                cv2.FONT_HERSHEY_SIMPLEX,
                                0.5,
                                (255, 255, 255))
        return det_img


if __name__ == "__main__":

    config_path = 'model/yolov3.cfg'
    weights_path = 'model/yolov3.weights'
    class_path = 'model/coco.names'
    img_size = 416
    conf_thres = 0.8
    nms_thres = 0.4
    od = ObjectDetector(config_path, weights_path, class_path, img_size)
    od.load_model()

    img_path = "data/15184_0.jpg"
    # img = Image.open(img_path)
    img = cv2.imread(img_path)
    detections = od.get_detections(img)
    det_img = od.show_detections(img, detections)
    cv2.imshow("Detections", det_img)
    cv2.waitKey(0)
