#!/usr/bin/env python3

import os
import urllib.request
import numpy as np
import cv2


# Disable caching and ignore all other policies
# Should be used in production according to the
# Google maps terms of use
DISABLE_CACHE = 0

# All previously unvisited locations will be recorded
# to use later on. Only for debug purposes.
RECORD_CACHE = 1

# Enables usage of locally cached images instead of
# API calls where it is possible
USE_CACHE = 2

# Forces to use only cached images, prevents API
# calls completely
USE_CACHE_ONLY = 3


def get_img_by_url(url):
    """ Downloads img, but doesn't save it, converts it into OpenCV format """
    resp = urllib.request.urlopen(url)
    image = np.asarray(bytearray(resp.read()), dtype="uint8")
    image = cv2.imdecode(image, cv2.IMREAD_COLOR)

    return image

class CacheProcessor:

    """
    Before API call is made CacheProcessor processes
    the request according to the list of policies,
    that can be used to reduce amount of API calls
    during development and debug.
    However for production cache policy should be set
    to DISABLE_CACHE and all previously collected cache
    should be removed.
    """

    def __init__(self, cache_dir, cache_policies: list):
        self.cache_policies = cache_policies
        self.cache_dir = cache_dir

        if len(cache_policies) == 0:
            raise ValueError('cache_policies can not be an empty list')

        if not os.path.exists(cache_dir):
            os.mkdir(cache_dir)

    def request_to_name(self, request_body, pano_id):
        """
        Squishes request into unique identifier,
        latitude and longitude parameters are being replaced
        with pano_id, which corresponds to given a position,
        it is useful since multiple pairs of coordinates
        can describe the same position, but we don't want to store
        cache for each possible pair of coordinates. Instead we just
        store cache for a position with given pano_id, orientation and size.
        """
        _cut = request_body.split('?')[-1]
        parameters = _cut.split('&')
        _main_parameters = '&'.join([p for p in parameters
                                   if not p.startswith('location')])
        name = pano_id + _main_parameters
        return name

    def name_to_request(self, name):
        """ Restores the whole request from cache name """
        pass

    def get_extension(self, request_type: str):
        """ Different kinds of API calls return different image extensions"""
        if request_type == 'staticmap':
            return 'png'
        if request_type == 'streetview':
            return 'jpg'

    def process_request(self, request: str, info: dict):

        # Get general info about request
        body = '&'.join(p for p in request.split('&') if not p.startswith('key'))
        pano_id = info['pano_id']
        request_type = body.split('?')[0].split('/')[-1]
        extension = self.get_extension(request_type)
        cache_dir = self.cache_dir
        fullpath = f'{cache_dir}/{request_type}'
        name = self.request_to_name(body, pano_id)
        file = f'{name}.{extension}'
        filepath = f'{fullpath}/{file}'

        # Retrieve data and skip all cache processing
        if DISABLE_CACHE in self.cache_policies:
            img = get_img_by_url(request)
            return img

        # Forces to use cached data, the great option
        # for poor plebs that we are
        if USE_CACHE_ONLY in self.cache_policies:
            # Sadly not implemented yet
            pass

        if RECORD_CACHE in self.cache_policies:

            if not os.path.exists(fullpath):
                os.mkdir(fullpath)

            cache_lst = os.listdir(fullpath)
            if not file in cache_lst:
                # Record new cache only
                urllib.request.urlretrieve(request, filepath)
                img = cv2.imread(filepath)
                return img

        if USE_CACHE in self.cache_policies:
            cache_lst = os.listdir(fullpath)
            if file in cache_lst:
                img = cv2.imread(filepath)
                return img
