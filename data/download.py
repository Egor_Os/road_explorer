import urllib.request
import cv2
import numpy as np
import os
from bs4 import BeautifulSoup


class DatasetLoader:

    def _check_dir(self, dirname, last_idx=0, max_idx=56000):
        self.dirname = dirname
        self.last_idx = last_idx
        self.max_idx = max_idx
        if os.path.exists(dirname):
            files_lst = sorted(os.listdir(dirname))
            if files_lst:
                last_name = files_lst[-1]
                self.last_idx = int(last_name.split('_')[0])
        else:
            os.mkdir(dirname)

    @staticmethod
    def get_img_by_url(url):
        resp = urllib.request.urlopen(url)
        image = np.asarray(bytearray(resp.read()), dtype="uint8")
        image = cv2.imdecode(image, cv2.IMREAD_COLOR)

        return image

    @staticmethod
    def resize_img(image, min_side):
        h, w, d = image.shape
        size = (w, h)
        if h < w:
            if h > min_side:
                new_h = int(min_side)
                new_w = int(w * min_side / h)
                size = (new_w, new_h)
        else:
            if w > min_side:
                new_w = int(min_side)
                new_h = int(h * min_side / w)
                size = (new_w, new_h)

        return cv2.resize(image, size, interpolation=cv2.INTER_AREA)

    def load_all(self, dirname, imsize=640):
        self._check_dir(dirname)
        last_idx = self.last_idx
        max_idx = self.max_idx
        for idx in range(last_idx, max_idx):
            page_url = f'https://dorogi-onf.ru/problem/{idx}/'
            response = urllib.request.urlopen(page_url)
            html = response.read()
            soup = BeautifulSoup(html, 'html.parser')
            links = soup.findAll('a')
            if links:
                sources = ['https://dorogi-onf.ru' + i['href'] for i in links
                           if i['href'].startswith('/media/feedback')]
                if sources:
                    # Remove first url as it is duplicate
                    if len(sources) > 1:
                        sources.pop(0)
                    for n, src in enumerate(sources):
                        img = self.get_img_by_url(src)
                        norm_img = self.resize_img(img, imsize)
                        filename = f'{self.dirname}/{idx}_{n}.jpg'
                        cv2.imwrite(filename, norm_img)
                        print(filename)

    def load_by_city_id(self, dirname, city_id, imsize=640):
        self._check_dir(dirname)
        page = 1
        url = f"https://dorogi-onf.ru/city/{city_id}/" \
                  f"?sort=-date&amp=&conditions=" \
                  f"&local=&query=&city={city_id}&page={page}"
        records = []

        # getting the number of the last page
        response = urllib.request.urlopen(url)
        html = response.read()
        soup = BeautifulSoup(html, 'html.parser')
        last_page = int(soup.find("a", {"title": "Последняя"})["data-page"])

        # gather all sources
        for page in range(1, last_page+1):
            url = f"https://dorogi-onf.ru/city/{city_id}/" \
                  f"?sort=-date&amp=&conditions=" \
                  f"&local=&query=&city={city_id}&page={page}"
            response = urllib.request.urlopen(url)
            html = response.read()
            soup = BeautifulSoup(html, 'html.parser')
            links = soup.findAll("a", {"class": "s-name"})
            if links:
                records += ['https://dorogi-onf.ru' + i['href'] for i in links]

        idx = 0
        for page_url in records:
            response = urllib.request.urlopen(page_url)
            html = response.read()
            soup = BeautifulSoup(html, 'html.parser')
            links = soup.findAll('a')
            if links:
                sources = ['https://dorogi-onf.ru' + i['href'] for i in links
                           if i['href'].startswith('/media/feedback')]
                if sources:
                    # Remove first url as it is duplicate
                    if len(sources) > 1:
                        sources.pop(0)
                    for src in sources:
                        img = self.get_img_by_url(src)
                        norm_img = self.resize_img(img, imsize)
                        filename = f'{self.dirname}/{idx}.jpg'
                        cv2.imwrite(filename, norm_img)
                        print(f"Downloaded {filename} from {src}")
                        idx += 1


if __name__ == "__main__":
    dl = DatasetLoader()
    dl.load_by_city_id('SPB', '4940')
